#!/usr/bin/env python3


## Byom Roulette 0.0.1


import requests
import sys
import random

proxylist = [
    { "http": "*********.**:*****" },
    { "https": "*********.**:*****" },
]


def file_array(file):
    with open(file, "r") as f:
        content = f.readlines()

    [line.rstrip('\n') for line in content]

    return content


def main(argv):
    
    # Get the args, first term to search, second the username list file, the third amount of threads.
    term     = argv[1]
    userlist = argv[2]


    usernamesarray = file_array(userlist)

    while True:
        name = random.choice(usernamesarray)
        req = "https://api.byom.de/mails/{}?alt=json-in-script&callback=angular.callback._1".format(name)
        try:
            print("[--] Testing username: {}".format(name))
            r = requests.get(req, headers={"content-type":"text"}, proxies=proxylist[0])
            for line in r:
                if term.encode() in line:
                    print("[!!] - FOUND: {} | with username: {}".format(term, name))
                    continue
        except Exception as e:
            print(e)
    

if __name__ == '__main__':
    main(sys.argv[0:])